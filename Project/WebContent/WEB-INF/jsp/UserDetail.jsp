<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

	<body>
        <header>ユーザ情報詳細参照</header>
        <p>
        <label>ログインID</label>
        ${user.loginId}</p>
        <p>
        <label>ユーザ名</label>
        ${user.name}</p>
        <p>
        <label>生年月日</label>
        ${user.birthDate}</p>
        <p>
        <label>登録日時</label>
        ${user.createDate}</p>
        <p>
        <label>更新日時</label>
        ${user.updateDate}</p>

         <div>
        <a href="UserListServlet">戻る</a>
        </div>



	</body>
</html>

