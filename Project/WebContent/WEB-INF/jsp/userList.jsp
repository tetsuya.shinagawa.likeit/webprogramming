<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <title>ユーザ一覧</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <head><a>${UserList.name}</a>
      <a href="LogoutServlet">ログアウト</a>
      </head>

     <body>
    <center><h1>ユーザ一覧</h1></center>

    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>

         <a href="NewUserServlet">新規登録</a>
 <form method="post" action="#" class="form-horizontal">
 <div>
  <div>
    <label for="colFormLabel" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input name="loginId" type="text" class="form-control" id="colFormLabel" >
    </div>
  </div>
  <div>
    <label for="colFormLabel" class="col-sm-2 col-form-label">ユーザ名</label>
    <div class="col-sm-10">
      <input name="name" type="text" class="form-control" id="colFormLabel">
    </div>
  </div>
  <div class="col-sm-10">
    <label for="colFormLabelLg" class="col-sm-2 col-form-label">生年月日</label>

      <input name="birthDate" type="date" class="form-control" id="folFormLabel">
      <a>~</a>
       <input name="birthDate2" type="date" class="form-control" id="folFormLabel">
      </div>

     <button type="submit" value="検索" class="btn btn-primary form-submit">検索
        </button>
</div>


</form>

         <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>

                      <a class="btn btn-primary" href="UserDetailServlet?id=${user.id} ">詳細</a>

                      <c:if test="${log.loginId == user.loginId}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>

                       <c:if test="${log.loginId == 'admin'}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                        </c:if>



                      <!--ここいじるらしい -->
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>




    </body>
