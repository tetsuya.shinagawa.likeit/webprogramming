<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous"/>

<head>
<center>
	<h1>ログイン画面</h1>
</center>
</head>
<body>
<div>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
 </div>

 <form class="form-signin" action="LoginServlet" method="post">
	<div class="form-group row">
		<label for="colFormLabel" class="col-sm-2 col-form-label">ログインID</label>

		<div class="col-sm-10">
			<input type="text" name="loginId" class="form-control" id="colFormLabel">
		</div>
	</div>
	<div class="form-group row">
		<label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
		<div class="col-sm-10">
			<input type="password" name="password" class="form-control"
				id="inputPassword" placeholder="パスワード"> <span
				id="reauth-email" class="reauth-email"></span>
			<center>
				<button class="btn btn-primary" type="submit">ログイン</button>
			</center>



		</div>
	</div>
		</form>
</body>