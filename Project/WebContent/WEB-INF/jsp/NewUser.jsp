<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
	<body>

    <header>ユーザ新規登録</header>
    <div>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
 </div>


 <form action="NewUserServlet" method="post">
  <div class="form-group row">
    <label for="colFormLabelSm" class="col-sm-2 col-form-label ">ログインID</label>
    <div class="col-sm-10">
      <input type="text" name="loginId" class="form-control" id="colFormLabelSm" placeholder="ログインID">
    </div>
  </div>
  <div class="form-group row">
    <label for="colFormLabel" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" id="colFormLabel" placeholder="パスワード">
    </div>
  </div>
  <div class="form-group row">
    <label for="colFormLabelLg" class="col-sm-2 col-form-label">パスワード（確認）</label>
    <div class="col-sm-10">
      <input type="password" name="password2" class="form-control" id="colFormLabelLg" placeholder="パスワード">
    </div>
  </div>

     <div class="form-group row">
    <label for="colFormLabelSm" class="col-sm-2 col-form-label ">ユーザ名</label>
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="colFormLabel" placeholder="ユーザ名">
    </div>
  </div>

     <div class="form-group row">
     <label for="colFormLabel" class="col-sm-2 col-form-label">誕生日</label>
     <div class="col-sm-10">
     <input type="date" name="birthDate" class="form control" id="col-form-label">
    </div>
     </div>
     <div>
  <input type="submit" value="登録">
  </div>

</form>
       <div>
        <a href="UserListServlet">戻る</a>
        </div>




	</body>
</html>

