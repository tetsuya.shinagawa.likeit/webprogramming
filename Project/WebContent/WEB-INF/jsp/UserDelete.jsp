<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>ユーザ削除確認</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
    <center>ユーザ名さん</center>
    <a href="LoginServlet">ログアウト</a>

<body>
    <header><h1>ユーザ削除確認</h1></header>
    <div>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	</div>

<form action="UserListServlet" method="get">

    <p>ログインID:${user.loginId}</p>
    <p>を本当に削除してよろしいでしょうか。</p>

    <input type="submit" value="キャンセル">
    </form>

    <form action="UserDeleteServlet" method="post">
    <input type="hidden" name="id" value="${user.id}">
    <input type="submit" value="ok">

</form>
    <div>
        <a href="UserListServlet">戻る</a>
        </div>
    </body>

    </html>
