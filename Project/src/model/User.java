package model;

import java.io.Serializable;



public class User implements Serializable {
private int id;
private String loginId;
private String name;
private String birthDate;
private String password;
private String createDate;
private String updateDate;
private String updateInfo;

public User() {
}

// ログインセッションを保存するためのコンストラクタ
public User(String loginId, String name) {
	this.loginId = loginId;
	this.name = name;
}




public User(int id, String loginId, String name, String birthDate, String createDate, String updateDate,String password) {
	super();
	this.id = id;
	this.loginId = loginId;
	this.name = name;
	this.birthDate = birthDate;
	this.createDate = createDate;
	this.updateDate = updateDate;
	this.password = password;
}




public User(String loginIdData, String nameData, String birthDateData, String createDateData, String updateDateData) {
	// TODO 自動生成されたコンストラクター・スタブ
	//userDetail用のコンストラクタ
	this.loginId=loginIdData;
	this.name=nameData;
	this.birthDate=birthDateData;
	this.createDate=createDateData;
	this.updateDate=updateDateData;
}



public User(int upId,String upPassword, String upName,String upBirthDate) {
	// TODO 自動生成されたコンストラクター・スタブ
	//updateUser用
	this.id=upId;
	this.password=upPassword;
	this.name=upName;
	this.birthDate=upBirthDate;

}



public User(String newLoginId, String newPassword, String newName, String newBirthDate) {
	// TODO 自動生成されたコンストラクター・スタブ
	//新規登録者用
	this.loginId=newLoginId;
	this.password=newPassword;
	this.name=newName;
	this.name=newBirthDate;

}

public User(String id) {
	// TODO 自動生成されたコンストラクター・スタブ
	//deleteUser
	this.loginId=id;
}


public User(String listLoginId, String listName, String listBirthDate) {
	// TODO 自動生成されたコンストラクター・スタブ
	//userListの値受け取り用
	this.loginId=listLoginId;
	this.name=listName;
	this.birthDate=listBirthDate;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getLoginId() {
	return loginId;
}
public void setLoginId(String loginId) {
	this.loginId = loginId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getBirthDate() {
	return birthDate;
}
public void setBirthDate(String birthDate) {
	this.birthDate = birthDate;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getCreateDate() {
	return createDate;
}
public void setCreateDate(String createDate) {
	this.createDate = createDate;
}
public String getUpdateDate() {
	return updateDate;
}
public void setUpdateDate(String updateDate) {
	this.updateDate = updateDate;
}
}
