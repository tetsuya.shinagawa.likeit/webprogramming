package dao;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {


	public List<User> findAll(){
		 Connection con = null;

		 List<User> userList = new ArrayList<User>();
		   try {
			   con=DBmanager.getConnection();//"jdbc:mysql://localhost:3306/usermanagement?useUnicode=true&characterEncoding=utf8", "root", "password"

			   // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id not in('admin');";

	            // SELECTを実行し、結果表（ResultSet）を取得
	            Statement stmt = con.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            while(rs.next()) {
	            	int id = rs.getInt("id");
	            	String loginId=rs.getString("login_id");
	            	String name=rs.getString("name");
	            	String birthDate=rs.getString("birth_date");
	            	String password = rs.getString("password");
	            	String createDate=rs.getString("create_date");
	            	String updateDate=rs.getString("update_date");

	            	User user = new User(id,loginId,name,birthDate,password,createDate,updateDate);
	            	userList.add(user);
	            }


		   } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();

	                }

	}
}
		   return userList;


}
	//これ繰り返しちゃってる

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String code) {
        Connection con = null;
        try {
            // データベースへ接続
            con = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, code);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String passwordData = rs.getString("password");
            return new User(loginIdData, passwordData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }



    public User findUserInfo(int id) {

        Connection con = null;
        try {
            // データベースへ接続
            con = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id=?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setInt(1, id);

            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int idData= rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            String birthDateData = rs.getString("birth_date");
            String createDateData = rs.getString("create_date");
            String updateDateData = rs.getString("update_date");
    		String passwordData=rs.getString("password");

            return new User(idData,loginIdData, nameData,birthDateData,createDateData,updateDateData,passwordData);


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    //うpデータ受け取り用
    public User updateInfo(String password,String name,String birthDate,int id) {
    	Connection con = null;
        try {
            // データベースへ接続
            con = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "UPDATE user SET password=?, name=?, birth_date=? WHERE id=?;";
            //sql error


             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setString(1, password);
            pStmt.setString(2, name);
            pStmt.setString(3, birthDate);
            pStmt.setInt(4, id);


            pStmt.executeUpdate();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う

            return new User();




        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
    //うpでパスワード両方空欄の場合
        public User updateInfo2(String name,String birthDate,int id) {
        	Connection con = null;
            try {
                // データベースへ接続
                con = DBmanager.getConnection();

                // SELECT文を準備
                String sql = "UPDATE user SET name=?, birth_date=? WHERE id=?;";
                //sql error


                 // SELECTを実行し、結果表を取得
                PreparedStatement pStmt = con.prepareStatement(sql);
                pStmt.setString(1, name);
                pStmt.setString(2, birthDate);
                pStmt.setInt(3, id);

                pStmt.executeUpdate();

                // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う

                return new User();




            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            } finally {
                // データベース切断
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            }


	}



    //新規登録者用
        public User findNewUserInfo(String loginId,String name,String birthDate,String password) {

            Connection con = null;
    try {

    	String sql ="INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,NOW(),NOW());";

        con = DBmanager.getConnection();   //Connectionクラス生成

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = con.prepareStatement(sql);

            pStmt.setString(1, loginId);
            pStmt.setString(2, name);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, password);

            pStmt.executeUpdate();

            return new User();


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
        public User deleteUser(int id) {

            Connection con = null;
    try {

    	String sql ="DELETE FROM user WHERE id=?";

        con = DBmanager.getConnection();   //Connectionクラス生成

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setInt(1, id);
            pStmt.executeUpdate();


            return new User();


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        }



            //NewUserServletに情報を送るため
            public String findLoginIdData(String loginId) {

            Connection con=null;
            try {

            	String sql ="SELECT login_id FROM user WHERE login_id = ?";

                con = DBmanager.getConnection();   //Connectionクラス生成

                     // SELECTを実行し、結果表を取得
                    PreparedStatement pStmt = con.prepareStatement(sql);
                    pStmt.setString(1, loginId);
                    ResultSet rs = pStmt.executeQuery();

                    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                    if (!rs.next()) {
                        return null;
                    }

                    // 必要なデータのみインスタンスのフィールドに追加
                    String loginIdList = rs.getString("login_id");


                    return loginIdList;


                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                } finally {
                    // データベース切断
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                }
            }

            public List<User> findUserListData(String loginId, String name, String birthDate, String birthDate2) {

                Connection con=null;

                List<User> userList = new ArrayList<User>();

                try {
                	String sql ="SELECT * FROM user WHERE login_id not in('admin')";

                	if(!loginId.equals("")) {
                		sql+=" AND login_id= "+"'"+loginId+"'";
                	}
                	if(!name.equals("")) {
                		sql+=" AND name LIKE "+"'"+ "%"+name+"%" +"'";

                	}
                	if(!birthDate.equals("")) {
                		sql+=" AND birth_date >= "+"'"+birthDate+"'";
                	}
                	if(!birthDate2.equals("")) {
                		sql+=" AND birth_date < "+"'"+birthDate2+"'";
                	}


                	System.out.println(sql);

                    con = DBmanager.getConnection();   //Connectionクラス生成

                         // SELECTを実行し、結果表を取得

						Statement stmt= con.createStatement();
                        ResultSet rs = stmt.executeQuery(sql);

                        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                        while(rs.next()) {

                        // 必要なデータのみインスタンスのフィールドに追加
                        String listLoginId = rs.getString("login_id");
                        String listName = rs.getString("name");
                        String listBirthDate = rs.getString("birth_date");

                        User user = new User(listLoginId ,listName,listBirthDate);
    	            	userList.add(user);

                        }



                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    } finally {
                        // データベース切断
                        if (con != null) {
                            try {
                                con.close();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    }
				return userList;
            }


            public String generate(String password) {
                MessageDigest md;
                String result ="" ;
				try {
					md = MessageDigest.getInstance("MD5");

                md.update(password.getBytes());
                byte[] digest = md.digest();
                 result = new BigInteger(1, digest).toString(16).toUpperCase();

				} catch (NoSuchAlgorithmException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				return result;
            }



}

//	public User findById(String id)
//	//戻り値を返す
//	{
//		Connection con=null;
//		try {
//			   con=DBmanager.getConnection();
//		 } catch (SQLException e) {
//	            e.printStackTrace();
//	        } finally {
//	            // データベース切断
//	            if (con != null) {
//	                try {
//	                    con.close();
//	                } catch (SQLException e) {
//	                    e.printStackTrace();
//
//	                }
//	}
//}
//	}

