package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User log = (User)session.getAttribute("log");

		if(log!=null) {

		String id = request.getParameter("id");
		int idInt = Integer.parseInt(id);


	// 確認用：idをコンソールに出力
	System.out.println(idInt);

	// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力す
	UserDao userDao=new UserDao();
	User user= userDao.findUserInfo(idInt);

	request.setAttribute("user",user);

	// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDelete.jsp");
	dispatcher.forward(request, response);
		}

	else {
		response.sendRedirect("LoginServlet");
	}
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");



		String id = request.getParameter("id");
		int idInt = Integer.parseInt(id);

		UserDao userDao=new UserDao();
		User user= userDao.deleteUser(idInt);

		request.setAttribute("user",user);
		response.sendRedirect("UserListServlet");

		/** テーブルに該当のデータが見つからなかった場合 **/
		if (user == null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインに失敗しました。");

	}
	}


}
