package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		int idInt = Integer.parseInt(id);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				UserDao userDao = new UserDao();
				User user = userDao.findUserInfo(idInt);

				request.setAttribute("user", user);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
				dispatcher.forward(request, response);
}


/**
 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
 */
protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");

	String upPassword = request.getParameter("password");
	String upPassword2 = request.getParameter("password2");
	String upName = request.getParameter("name");
	String upBirthDate = request.getParameter("birthDate");
	String upId = request.getParameter("id");
	int idInt = Integer.parseInt(upId);

	//パスワードをmd５で暗号化
			UserDao userDao = new UserDao();

			request.setAttribute(upPassword,"password");
			String code = userDao.generate(upPassword);

			request.setAttribute(upPassword2,"password2");
			String codeConf = userDao.generate(upPassword);



	if(!upPassword.equals(upPassword2)) {
		request.setAttribute("errMsg", "パスワードが一致しません");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
		dispatcher.forward(request, response);
		return;
	}
	else if(upName.equals("")||upBirthDate.equals("")||upId.equals("")) {
		request.setAttribute("errMsg", "未記入の項目があります");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
		dispatcher.forward(request, response);
		return;
		}

	else if(upPassword.equals("")) {
		User user = userDao.updateInfo2(upName,upBirthDate,idInt);
		request.setAttribute("user", user);
		response.sendRedirect("UserListServlet");

	}
	else {

	User user = userDao.updateInfo(code,upName,upBirthDate,idInt);

	request.setAttribute("user", user);
	response.sendRedirect("UserListServlet");

	}




}
}





